from probabilistic_verification import SampleGenerator
from fault_injection import FaultInjector
from adversarial_attack import AdversarialAttack, FGSM_attack
from adversarial_attack import Few_pixel_attack, All_pixel_attack 
import tensorflow as tf
import numpy as np
from typing import Callable
import random

class FaultResilienceSample( SampleGenerator ):

    def __init__(self, model_path: str         = None, 
                 input_preprocess : Callable   = None,
                 data_train : np.ndarray       = None, 
                 label_train: np.ndarray       = None,
                 data_test: np.ndarray         = None,  
                 label_test: np.ndarray        = None,                
                 batch_size: int               = 64, 
                 max_steps: int                = 100,
                 fault_injector: FaultInjector = None):
        super().__init__("sample of fault resilience bellow threshold")
        self.model_path       = model_path
        self.input_preprocess = input_preprocess
        self.data_train       = data_train
        self.data_test        = data_test
        self.label_train      = label_train
        self.label_test       = label_test
        self.batch_size       = batch_size
        self.max_steps        = max_steps
        self.train_images_preprocessed = self.input_preprocess(self.data_train)
        self.test_images_preprocessed  = self.input_preprocess(self.data_test)
        model                          = tf.keras.models.load_model( self.model_path )
        _, self.ref_test_acc           = model.evaluate(self.test_images_preprocessed, 
                                                        self.label_test, verbose=0)
        self.fault_injector            = fault_injector
        self.threshold                 = None

    def set_resilience_threshold(self, threshold: float) -> None :
        self.threshold = threshold

    def __perturb(self,) -> None :
        self.perturbed_model = tf.keras.models.load_model( self.model_path ) 
        self.fault_injector.inject( self.perturbed_model )
        test_loss, self.test_acc_perturb = self.perturbed_model.evaluate(self.test_images_preprocessed, self.label_test, verbose=0)

    def __recovery(self,) -> None:
        self.recovery_curv = self.perturbed_model.fit(
            self.train_images_preprocessed, 
            self.label_train,
            batch_size=self.batch_size,
            steps_per_epoch = 1,
            validation_data=(self.test_images_preprocessed, self.label_test), 
            validation_batch_size = int(len(self.label_test)/2),
            validation_steps = 1,
            epochs=self.max_steps,
            shuffle=True,
            verbose=0).history['val_accuracy'] 
        
    def __curve_preprocessing(self,) -> None:
        max_value = self.recovery_curv[0]
        for i in range(len(self.recovery_curv)):
            if self.recovery_curv[i] > max_value :
                max_value = self.recovery_curv[i]
            if self.recovery_curv[i] < max_value : 
                self.recovery_curv[i] = max_value
        self.recovery_curv = [self.ref_test_acc]+[self.test_acc_perturb]+self.recovery_curv   

    def __compute_resilience_metric(self,) -> float: 
        Pnorm = self.recovery_curv[0]
        Tc    = len(self.recovery_curv)-1
        Rnorm = Pnorm*Tc
        R     = sum( self.recovery_curv[1:] )
        if  self.recovery_curv[-1] < 0.95*Pnorm :
            return 0
        return R/Rnorm

    def __get_sample_resilience(self, N: int) -> np.ndarray : 
        r_sample = []
        for i in range(N):
            self.__perturb()
            self.__recovery()
            self.__curve_preprocessing()
            r_sample.append( self.__compute_resilience_metric() )
        return np.array(r_sample)

    def get_sample(self, N: int) -> np.ndarray :
        if self.threshold is None :
            raise Exception("You should set threshold for resilience indicator") 
        r_sample = self.__get_sample_resilience( N )
        return (r_sample < self.threshold).astype(np.int_)

class AdversarialResilienceSample( SampleGenerator ):

    def __init__(self, model_path: str       = None, 
                 input_preprocess : Callable = None,
                 data_train : np.ndarray     = None, 
                 label_train: np.ndarray     = None,
                 data_test: np.ndarray       = None,  
                 label_test: np.ndarray      = None,                
                 batch_size: int             = 64, 
                 max_steps: int              = 100,
                 attacker: AdversarialAttack = 5):
      
        super().__init__("sample of fault resilience bellow threshold")

        self.model_path       = model_path
        self.input_preprocess = input_preprocess
        self.data_train       = data_train
        self.data_test        = data_test
        self.label_train      = label_train
        self.label_test       = label_test
        self.batch_size       = batch_size
        self.max_steps        = max_steps
        self.attacker         = attacker

        self.model                     = tf.keras.models.load_model( self.model_path )
        self.train_images_preprocessed = self.input_preprocess(self.data_train)
        self.test_images_preprocessed  = self.input_preprocess(self.data_test)
        _, self.ref_test_acc = self.model.evaluate(self.test_images_preprocessed, 
                                                                    self.label_test, 
                                                                    verbose=0)
        self.threshold = None

    def set_resilience_threshold(self, threshold: float) -> None :
        self.threshold = threshold

    def __perturb(self,) -> None :
        self.model = tf.keras.models.load_model( self.model_path ) 

        indexes = np.arange(len(self.data_train))
        np.random.shuffle(indexes)
        train_num = self.batch_size*self.max_steps
        adv_train_image = self.attacker.generate_adv_image( self.data_train[ indexes[0:train_num] ] )
        self.adv_train_image_preprocessed = self.input_preprocess(adv_train_image)
        self.train_label_preprocessed = self.label_train[ indexes[0:train_num] ]

        indexes = np.arange(len(self.data_test))
        np.random.shuffle(indexes)
        test_num = int(len(self.data_test)/3)
        adv_test_image = self.attacker.generate_adv_image( self.data_test[ indexes[0:test_num] ] )
        self.adv_test_image_preprocessed = self.input_preprocess(adv_test_image)
        self.test_label_preprocessed = self.label_test[ indexes[0:test_num] ]

        _, self.test_acc_perturb = self.model.evaluate(self.adv_test_image_preprocessed, 
                                                               self.test_label_preprocessed, 
                                                               verbose=0)

    def __recovery(self,) -> None:
        self.recovery_curv = self.model.fit( 
            self.adv_train_image_preprocessed, 
            self.train_label_preprocessed, 
            batch_size=self.batch_size,
            steps_per_epoch = 1,
            validation_data=(self.adv_test_image_preprocessed, self.test_label_preprocessed), 
            validation_batch_size = len(self.test_label_preprocessed),  
            validation_steps = 1,
            epochs=self.max_steps,
            verbose=0).history['val_accuracy'] 
        
    def __curve_preprocessing(self,) -> None:
        max_value = self.recovery_curv[0]
        for i in range(len(self.recovery_curv)):
            if self.recovery_curv[i] > max_value :
                max_value = self.recovery_curv[i]
            if self.recovery_curv[i] < max_value : 
                self.recovery_curv[i] = max_value
        self.recovery_curv = [self.ref_test_acc]+[self.test_acc_perturb]+self.recovery_curv   

    def __compute_resilience_metric(self,) -> float: 
        Pnorm = self.recovery_curv[0]
        Tc    = len(self.recovery_curv)-1
        Rnorm = Pnorm*Tc
        R     = sum( self.recovery_curv[1:] )
        if  self.recovery_curv[-1] < 0.95*Pnorm :
            return 0
        return R/Rnorm

    def __get_sample_resilience(self, N: int) -> np.ndarray : 
        r_sample = []
        for i in range(N):
            self.__perturb()
            self.__recovery()
            self.__curve_preprocessing()
            r_sample.append( self.__compute_resilience_metric() )
        return np.array(r_sample)

    def get_sample(self, N: int) -> np.ndarray :
        if self.threshold is None :
            raise Exception("You should set threshold for resilience indicator") 
        r_sample = self.__get_sample_resilience( N )
        return (r_sample < self.threshold).astype(np.int_)