import os
import tensorflow as tf
from struct import pack, unpack
import numpy as np
from tensorflow.keras import Model, layers
from tensorflow.keras import backend as K
import random, math

## 1. Target  
#### Two kinds of injection are possible with TensorFI 2. 
#### These are static and dynamic injections. 
#### We specify this using the Target label, as it depends on what artifact we inject as well.
####  1.1. layer_states: The weights and biases of each layer compose of its state and layer_states 
####       FI is static and affect the stored state values of the chosen layer.
####  1.2. layer_outputs: The final computations or activations of each layer compose of its output and layer_outputs FI is dynamic. 
####       This is analogous to operator output injections (the only kind of injection) in TensorFI 1.
## 2. Mode
#### Two kinds of injection modes can be specified in TensorFI 2. These are single and multiple.
####  2.1. single: When this mode is selected, only one layer out of the total n model layers is randomly chosen for injection.
####  2.2. multiple: Presently, multiple mode means that each and every layer is selected for injection.
## 3. Type
#### Three types of faults can be injected with TensorFI 2. 
#### These are zeros, random value replacements and bitflips.
####  3.1. zeros: The chosen tensor value(s) are replaced with zeros.
####  3.2. random: The chosen tensor value(s) are replaced with random value(s) in the range [0,1).
####  3.3. bitflips: Bits are flipped in the chosen tensor value(s). 
####       When bitflips fault type is chosen, the bits to be flipped can be chosen randomly during injection or configured before injection.
## 4. Amount
#### The Amount of faults are of two types - either the exact number or a percentage.
####  4.1. For bitflips and random value replacement fault types, Amount is the exact number of values to be injected with the specified fault. 
####       As an example, if 10 is specified as the Amount for bitflips, then 10 tensor values are randomly chosen of that particular layer 
####       under consideration and 1 bit is flipped in each of the 10 values. Thus the range of allowed values are integers between 0 and 
####       total number of the tensor values of the layer state or output under consideration.
####  4.2. For zeros, Amount is the percentage of total tensor values and varies from 0% to 100%. 
####       As an example, if 10 is specified as the Amount for zeros, and there are 1000 values in that 
####       particular layer state or output tensor under consideration, then 100 tensor values are replaced with zeros.
## 5. Bit
#### The Bit is specified only if bitflips fault type are chosen. 
#### If the user wants the bit to be flipped to be chosen randomly by the injector during runtime, N is specified. 
#### Otherwise, the bit position to be flipped can be indicated with a value between 0 and 31. 
#### This is because we assume the models we consider use float32 precision.

class FaultInjector():
    def __init__(self, target="layer_states", mode="single", amount = 1, bit="N", ftype="bitflips", x_test=None):
        # Retrieve config params  
        self.target = target
        self.amount = amount
        self.bit    = bit
        self.mode   = mode
        self.ftype  = ftype 
        self.x_test = x_test
        #self.Model  = model # No more passing or using a session variable in TF v2
        # Call the corresponding FI function

             
    def inject(self, model):
        if self.target=="layer_states":
            self.layer_states(model)
        else :
            self.layer_outputs(model)
        
    def bitflip(self, f, pos):
        """ Single bit-flip in 32 bit floats """
        f_ = pack('f', f)
        b = list(unpack('BBBB', f_))
        [q, r] = divmod(pos, 8)
        b[q] ^= 1 << r
        f_ = pack('BBBB', *b)
        f = unpack('f', f_)
        return f[0]

    def layer_states(self, model):
        """ FI in layer states """
        if(self.mode  == "single"):
            """ Single layer fault injection mode """
            #print("Starting fault injection in a random layer")
            # Retrieve type and amount of fault
            fiFault = self.ftype
            fiSz = self.amount
            # Choose a random layer for injection
            randnum = random.randint(0, len(model.trainable_variables) - 1)
            # Get layer states info
            v = model.trainable_variables[randnum]
            num = v.shape.num_elements()
            if(fiFault == "zeros"):
                fiSz = (fiSz * num) / 100
                fiSz = math.floor(fiSz)
            # Choose the indices for FI
            ind = random.sample(range(num), fiSz)
            # Unstack elements into a single dimension
            elem_shape = v.shape
            v_ = tf.identity(v)
            v_ = tf.keras.backend.flatten(v_)
            v_ = tf.unstack(v_)
            # Inject the specified fault into the randomly chosen values
            if(fiFault == "zeros"):
                for item in ind:
                    v_[item] = 0.
            elif(fiFault == "random"):
                for item in ind:
                    v_[item] = np.random.random()
            elif(fiFault == "bitflips"):
                for item in ind:
                    val = v_[item]
                    # If random bit chosen to be flipped
                    if(self.bit == "N"):
                        pos = random.randint(0, 31)
                    # If bit position specified for flip
                    else:
                        pos = int(self.bit)
                    val_ = self.bitflip(val, pos)
                    v_[item] = val_
            # Reshape into original dimensions and store the faulty tensor
            v_ = tf.stack(v_)
            v_ = tf.reshape(v_, elem_shape)
            v.assign(v_)
            #print("Completed injections... exiting")

        elif(self.mode == "multiple"):
            """ Multiple layer fault injection mode """
            #print("Starting fault injection in all layers")
            # Retrieve type and amount of fault
            fiFault = self.ftype
            fiSz = self.amount
            # Loop through each available layer in the model
            for n in range(len(model.trainable_variables) - 1):
                # Get layer states info
                v = model.trainable_variables[n]
                num = v.shape.num_elements()
                if(fiFault == "zeros"):
                    fiSz = (fiSz * num) / 100
                    fiSz = math.floor(fiSz)
                # Choose the indices for FI
                ind = random.sample(range(num), fiSz)
                # Unstack elements into a single dimension
                elem_shape = v.shape
                v_ = tf.identity(v)
                v_ = tf.keras.backend.flatten(v_)
                v_ = tf.unstack(v_)
                # Inject the specified fault into the randomly chosen values
                if(fiFault == "zeros"):
                    for item in ind:
                        v_[item] = 0.
                elif(fiFault == "random"):
                    for item in ind:
                        v_[item] = np.random.random()
                elif(fiFault == "bitflips"):
                    for item in ind:
                        val = v_[item]
                        # If random bit chosen to be flipped
                        if(self.bit == "N"):
                            pos = random.randint(0, 31)
                        # If bit position specified for flip
                        else:
                            pos = int(self.bit)
                        val_ = self.bitflip(val, pos)
                        v_[item] = val_
                # Reshape into original dimensions and store the faulty tensor
                v_ = tf.stack(v_)
                v_ = tf.reshape(v_, elem_shape)
                v.assign(v_)
            #print("Completed injections... exiting")


    def layer_outputs(self, model):
        """ FI in layer computations/outputs """
        if(self.mode == "single"):
            """ Single layer fault injection mode """
            #print("Starting fault injection in a random layer")
            # Retrieve type and amount of fault
            fiFault = self.type
            fiSz = self.amount
            # Choose a random layer for injection
            randnum = random.randint(0, len(model.layers) - 2)
            fiLayer = model.layers[randnum]
            # Get the outputs of the chosen layer
            get_output = K.function([model.layers[0].input], [fiLayer.output])
            fiLayerOutputs = get_output([self.x_test]) 
            # Unstack elements into a single dimension
            elem_shape = fiLayerOutputs[0].shape
            fiLayerOutputs[0] = fiLayerOutputs[0].flatten()
            num = fiLayerOutputs[0].shape[0]
            if(fiFault == "zeros"):   
                fiSz = (fiSz * num) / 100
                fiSz = math.floor(fiSz)
            # Choose the indices for FI
            ind = random.sample(range(num), fiSz)
            # Inject the specified fault into the randomly chosen values
            if(fiFault == "zeros"):
                for item in ind:
                    fiLayerOutputs[0][item] = 0.
            elif(fiFault == "random"): 
                for item in ind:
                    fiLayerOutputs[0][item] = np.random.random()
            elif(fiFault == "bitflips"): 
                for item in ind:
                    val = fiLayerOutputs[0][item]
                    if(self.bit == "N"): 
                        pos = random.randint(0, 31)
                    else:
                        pos = int(self.bit)
                    val_ = self.bitflip(val, pos)
                    fiLayerOutputs[0][item] = val_
            # Reshape into original dimensions and get the final prediction
            fiLayerOutputs[0] = fiLayerOutputs[0].reshape(elem_shape)
            get_pred = K.function([model.layers[randnum + 1].input], [model.layers[-1].output])
            pred = get_pred([fiLayerOutputs])
            # Uncomment below line and comment next two lines for ImageNet models
            # return pred
            labels = np.argmax(pred, axis=-1)
            return labels[0]
            #print("Completed injections... exiting")
        elif(self.mode == "multiple"): 
            """ Multiple layer fault injection mode """
            #print("Starting fault injection in all layers")
            # Retrieve type and amount of fault
            fiFault = self.type   
            fiSz = self.amount  
            # Get the outputs of the first layer
            get_output_0 = K.function([model.layers[0].input], [model.layers[1].output])
            fiLayerOutputs = get_output_0([self.x_test]) 
            # Loop through each available layer in the model
            for n in range(1, len(model.layers) - 2):
                # Unstack elements into a single dimension
                elem_shape = fiLayerOutputs[0].shape
                fiLayerOutputs[0] = fiLayerOutputs[0].flatten()
                num = fiLayerOutputs[0].shape[0]
                if(fiFault == "zeros"): 
                    fiSz = (fiSz * num) / 100
                    fiSz = math.floor(fiSz)
                # Choose the indices for FI
                ind = random.sample(range(num), fiSz)
                # Inject the specified fault into the randomly chosen values
                if(fiFault == "zeros"): 
                    for item in ind:
                        fiLayerOutputs[0][item] = 0.
                elif(fiFault == "random"):
                    for item in ind:
                        fiLayerOutputs[0][item] = np.random.random()
                elif(fiFault == "bitflips"): 
                    for item in ind:
                        val = fiLayerOutputs[0][item]
                        if(self.bit == "N"): 
                            pos = random.randint(0, 31)
                        else:
                            pos = int(self.bit) 
                        val_ = self.bitflip(val, pos)
                        fiLayerOutputs[0][item] = val_
                # Reshape into original dimensions
                fiLayerOutputs[0] = fiLayerOutputs[0].reshape(elem_shape)
                """
                Check if last but one layer reached;
                if not, replace fiLayerOutputs with the next prediction to continue
                """
                if(n != (len(model.layers) - 3)):
                    get_output = K.function([model.layers[n+1].input], [model.layers[n+2].output])
                    fiLayerOutputs = get_output([fiLayerOutputs])
                # Get final prediction
                get_pred = K.function([model.layers[len(model.layers)-1].input], [model.layers[-1].output])
                pred = get_pred([fiLayerOutputs])
                # Uncomment below line and comment next two lines for ImageNet models
                # return pred
                labels = np.argmax(pred, axis=-1)
                return labels[0]
                #print("Completed injections... exiting")            