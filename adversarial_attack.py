# !pip install deap==1.2.2
import deap.creator, deap.tools, deap.base, deap.algorithms, deap.cma
from scipy.optimize import fsolve
import matplotlib.pyplot as plt
from typing import Callable
import tensorflow as tf
from enum import Enum
import numpy as np
import random
import cv2

class Norm(Enum):
    L0 = 0
    L1 = 1
    L2 = 2
    Linf = np.inf

def compute_norm(delta:np.array, ord:Norm) -> float:
    #0, 1, 2, np.inf
    if ord == Norm.L0 :
        return np.linalg.norm(delta, 0)
    if ord == Norm.L1 :
        return np.linalg.norm(delta, 1)
    if ord == Norm.L2 :
        return np.linalg.norm(delta, 2)
    if ord == Norm.Linf :
        return np.linalg.norm(delta, np.inf)

def compute_coefficient(delta:float, ord:Norm, target_level:float) -> float:
    func = lambda alpha : compute_norm(alpha*delta, ord)-target_level
    return fsolve(func, 0.0)[0]

class AdversarialAttack:
    def __init__(self, name: str):
        self.name = name

    def generate_adv_image(self, target_img:np.array) -> np.array :
        raise NotImplementedError("Should have implemented this")

class FGSM_attack ( AdversarialAttack ):
    def __init__(self, 
                 model_path : str            = None, 
                 input_preprocess : Callable = None,
                 perturbation_level : float  = 0.1):
        super().__init__("Fast Gradient Sign Method")
        self.target_model     = tf.keras.models.load_model( model_path )
        self.preprocess_input = input_preprocess
        self.epsilon          = perturbation_level
        self.loss_object      = tf.keras.losses.CategoricalCrossentropy()
        
    def __preprocess_input(self, image : np.array) -> tf.Tensor:
        preprocessed = self.preprocess_input(image)
        return tf.cast(preprocessed, tf.float32)

    # Helper function to extract labels from probability vector
    def __preprocess_output( self, probs : tf.Tensor ) -> tf.Tensor:
        index = tf.argmax(probs, 1)
        # print("index = ", index )
        label = tf.one_hot(index, probs.shape[-1])
        return label

    def __create_adv_pattern(self, input_image : tf.Tensor, input_label : tf.Tensor) -> tf.Tensor:
        with tf.GradientTape() as tape:
            tape.watch(input_image)
            prediction = self.target_model(input_image)
            loss = self.loss_object(input_label, prediction)
        # Get the gradients of the loss w.r.t to the input image.
        gradient = tape.gradient(loss, input_image)
        # Get the sign of the gradients to create the perturbation
        signed_grad = tf.sign(gradient)
        return signed_grad

    def generate_adv_image(self, target_img:np.array) -> np.array:
        image = self.__preprocess_input(target_img)
        image_probs = self.target_model.predict(image)
        label = self.__preprocess_output(image_probs)
        if target_img.ndim < 4 :
            perturbations = self.__create_adv_pattern(image, label).numpy()[0]
        else :
            perturbations = self.__create_adv_pattern(image, label).numpy()
        adv_x = target_img + self.epsilon*perturbations
        adv_x = np.clip(adv_x, 0, 255)
        image = self.__preprocess_input(adv_x)
        image_probs = self.target_model.predict(image)
        label = self.__preprocess_output(image_probs)
        return adv_x

class Few_pixel_attack (AdversarialAttack):

    def __init__(self, 
                 model_path : str            = None, 
                 preprocess_input : Callable = None, 
                 target_class : int          = None, 
                 num_pixels : int            = 1, 
                 num_gen : int               = 500, 
                 sigma : float               = 0.3, 
                 verbose : bool              = False):
        super().__init__("Black-Box CMA-ES based few pixel (L0) attack")
        self.target_model     = tf.keras.models.load_model( model_path )
        self.preprocess_input = preprocess_input
        deap.creator.create("FitnessMax", deap.base.Fitness, weights=(1.0,))
        deap.creator.create("Individual", np.ndarray, fitness=deap.creator.FitnessMax)
        self.toolbox          = deap.base.Toolbox()
        self.target_class     = target_class
        self.num_pixels       = num_pixels
        self.num_gen          = num_gen
        self.sigma            = sigma
        self.verbose          = verbose

    def add_adv(self, adv):
        adv = np.clip(adv, 0, 1)
        adv_img = cv2.resize(self.target_img, 
                             (self.width, self.height), 
                             interpolation = cv2.INTER_AREA)
        # each modification of 1 pixel includes (x, y, dr, dg, gb) 
        # which are position and channel values of the pixel we want to modify
        for i in range(0, self.num_pixels * 5, 5):
            x = int(adv[i] * (self.width-1) ) 
            y = int(adv[i+1] * (self.height-1)) 
            dr, dg, db = (adv[i+2:i+5] * 255).astype(np.uint8)
            adv_img[x, y, 0] = dr
            adv_img[x, y, 1] = dg
            adv_img[x, y, 2] = db
        return adv_img
    
    def evaluate_batch(self, individuals):
        individuals = np.clip(individuals, 0, 1)
        m = len(individuals)
        adv_imgs = np.zeros((m, self.target_img.shape[0], 
                                self.target_img.shape[1], 
                                self.target_img.shape[2])) 
        for i, individual in enumerate(individuals):
            adv_imgs[i] = self.add_adv(individual)
        if self.preprocess_input is not None :
             processed  = self.preprocess_input(adv_imgs) 
        else :
             processed  = adv_imgs
        pred = self.target_model.predict( processed )
        success = False
        if self.target_class is None: # non-targeted attack
            label_confidences = pred[np.arange(m), self.predicted_class]
            pred[np.arange(m), self.predicted_class] = 0
            attack_confidences = np.max(pred, axis=1)
            for i in range(m):
                if attack_confidences[i] > label_confidences[i]:
                    success = True
            return attack_confidences - label_confidences, success
        else: # targeted attack
            attack_confidences = pred[np.arange(m), self.target_class]
            pred[np.arange(m), self.target_class] = 0
            other_confidences = np.max(pred, axis=1)
            return np.log(attack_confidences) - np.log(other_confidences)
            
    def cma_es_search(self, num_gen, sigma=0.5, verbose=True):
        strategy = deap.cma.Strategy(self.toolbox.individual(), sigma=sigma, lambda_=64)
        self.toolbox.register("generate", strategy.generate, deap.creator.Individual)
        self.toolbox.register("update", strategy.update)
        self.hof = deap.tools.HallOfFame(1, similar=np.array_equal)
        stats = deap.tools.Statistics(lambda ind: ind.fitness.values)
        stats.register("avg", np.mean)
        stats.register("std", np.std)
        stats.register("min", np.min)
        stats.register("max", np.max)
        logbook = deap.tools.Logbook()
        for gen in range(1, num_gen + 1):
            population = self.toolbox.generate()
            fitnesses, success = self.evaluate_batch(population)
            for ind, fit in zip(population, fitnesses):
                ind.fitness.values = (fit,)
            self.hof.update(population)
            self.toolbox.update(population)
            record = stats.compile(population) if stats is not None else {}
            logbook.record(gen=gen, nevals=len(population), **record)
            if verbose:
                print(logbook.stream)
            if success:
                return True
        return False

    def __generate_adv_image(self, target_img : np.array ) -> np.array:
        self.toolbox.register("individual", 
                              deap.tools.initRepeat, 
                              container=deap.creator.Individual, 
                              func=lambda: random.random(), 
                              n=self.num_pixels*5)
        self.toolbox.register("population", 
                              deap.tools.initRepeat, 
                              list, 
                              self.toolbox.individual)
        self.target_img = target_img
        if self.preprocess_input is not None :
             processed = self.preprocess_input(self.target_img) 
        else :
             processed = np.array( [self.target_img] ) 
        self.width = processed.shape[2]
        self.height = processed.shape[1]        
        self.predicted_class = np.argmax(self.target_model.predict( processed )[0])
        self.cma_es_search(self.num_gen, self.sigma, self.verbose)
        adv_img = self.add_adv(self.hof[0])
        adv_img = np.clip(adv_img, 0, 255)
        return adv_img.astype(np.uint8)

    def generate_adv_image(self, target_img : np.array ) -> np.array:
        if target_img.ndim < 4 :
            return self.__generate_adv_image(target_img)
        else :
            result = []
            for i in range (len(target_img)):
                result.append(self.__generate_adv_image( target_img[i] ))
            return np.array(result)     

class All_pixel_attack (AdversarialAttack) :

    def __init__(self, 
                 model_path : str            = None, 
                 preprocess_input : Callable = None,
                 target_class : int          = None, 
                 constraint : float          = 3,
                 num_gen : int               = 10,
                 sigma : float               = 0.3,
                 verbose : bool              = False):
        super().__init__("Black-Box CMA-ES based all pixel (Linf) attack")
        self.target_model     = tf.keras.models.load_model( model_path )
        self.preprocess_input = preprocess_input
        deap.creator.create("FitnessMax", deap.base.Fitness, weights=(1.0,))
        deap.creator.create("Individual", np.ndarray, fitness=deap.creator.FitnessMax)
        self.toolbox = deap.base.Toolbox()
        self.constraint       = constraint/255
        self.num_gen          = num_gen
        self.sigma            = sigma
        self.verbose          = verbose

    # check if a candidate is valid
    def is_feasible(self, individual):
        return np.max(np.abs(individual)) <= self.constraint
    
    # add perturbation to target image
    def add_adv(self, adv):
        if len(adv.shape) < 3:
            adv = adv.reshape(self.target_img.shape)
        # clip if the magnitude of perturbation excesses constraint
        if not self.is_feasible(adv):
            adv = np.clip(adv, -self.constraint, self.constraint)
        adv_img = self.target_img / 255.0 + adv 
        adv_img = np.clip(adv_img, 0, 1)         
        adv_img = adv_img * 255.0                
        # make sure that all pixels of image are integer number
        adv_img = np.rint(adv_img)
        return adv_img

    # evaluate the fitnesses of all individuals on population 
    def general_evaluate_batch(self, individuals):
        if isinstance(individuals, list):
            individuals = np.array(individuals)
        m = individuals.shape[0]
        if not self.is_feasible(individuals):
            individuals = np.clip(individuals, -self.constraint, self.constraint)
        adv_img = self.target_img / 255.0 + individuals
        adv_img = np.clip(adv_img, 0, 1)
        adv_img = adv_img * 255.0
        # make sure that all pixels of image are integer number
        adv_img = np.rint(adv_img)
        if self.preprocess_input is not None :
             processed           = self.preprocess_input(adv_img) 
        else :
             processed           = np.copy(adv_img) 
        pred = self.target_model.predict(processed)
        max_top5_confidence = np.zeros(m)
        for predicted_idx in self.predicted:
            max_top5_confidence = np.maximum(max_top5_confidence, pred[np.arange(m), predicted_idx])
            pred[np.arange(m), predicted_idx] = 0     
        max_confidence_non_target = np.max(pred, axis=1)
        return np.log(max_confidence_non_target) - np.log(max_top5_confidence)

    def evaluate_batch(self, individuals):
        m = len(individuals)
        advs = np.zeros(shape=(m, self.height, self.width, self.target_img.shape[2]))
        for i, individual in enumerate(individuals):
            advs[i] = self.get_adv(individual)
        return self.general_evaluate_batch(advs)

    def set_base_vectors(self, base_vectors=None, num_base_vectors=None):
        self.flag_toolbox = True
        if base_vectors is None:
            if num_base_vectors is not None:
                self.num_base_vectors = num_base_vectors 
            else:
                self.num_base_vectors = 128
            self.base_vectors = np.random.uniform(-self.constraint, 
                                                  self.constraint, 
                                                  (self.height * self.width * self.target_img.shape[2], self.num_base_vectors) ) #!!!!!!!!!!!!!!!
        else:
            self.base_vectors = base_vectors
            self.num_base_vectors = self.base_vectors.shape[0]
        self.setup_toolbox()
   
    def set_init_point(self, init_point):
        self.init_point = self.toolbox.individual()
        if init_point is not None:
            for i, v in enumerate(init_point):
                self.init_point[i] = v
   
    def setup_toolbox(self):
        self.toolbox.register("individual", deap.tools.initRepeat, 
                         container=deap.creator.Individual, func=lambda: np.random.uniform(-1.0/self.num_base_vectors, 1.0/self.num_base_vectors), 
                         n=self.num_base_vectors)
        self.toolbox.register("population", deap.tools.initRepeat, list, self.toolbox.individual)
   
    def get_adv(self, individual):
        return np.sum(np.multiply(self.base_vectors, individual), axis=1).reshape( (self.height, self.height, self.target_img.shape[2]) ) 
    
    def evol(self, max_gen, sigma=0.2, verbose=True, num_base_vectors=128):
        self.setup_toolbox()
        self.set_base_vectors(num_base_vectors=num_base_vectors)
        strategy = deap.cma.Strategy(self.toolbox.individual(), sigma=sigma)
        self.toolbox.register("generate", strategy.generate, deap.creator.Individual)
        self.toolbox.register("update", strategy.update)
        self.hof = deap.tools.HallOfFame(1, similar=np.array_equal)
        stats = deap.tools.Statistics(lambda ind: ind.fitness.values)
        stats.register("avg", np.mean)
        stats.register("std", np.std)
        stats.register("min", np.min)
        stats.register("max", np.max)
        logbook = deap.tools.Logbook()
        logbook.header = ['gen', 'nevals'] + stats.fields
        for gen in range(1, max_gen + 1, 1):
            # Generate a new population
            population = self.toolbox.generate()
            # Evaluate the individuals
            fitnesses = self.evaluate_batch(population)#
            for ind, fit in zip(population, fitnesses):
                ind.fitness.values = (fit,)
            # update hall of fame
            self.hof.update(population)
            # Update the strategy with the evaluated individuals
            self.toolbox.update(population)
            record = stats.compile(population) if stats is not None else {}
            logbook.record(gen=gen, nevals=len(population), **record)
            if verbose:
                print(logbook.stream)
            if self.hof[0].fitness.values[0] > 0:
                return gen
        return -1
        
    def __generate_adv_image(self, target_img ):
        
        self.target_img = target_img
        self.target_class = self.target_class
        if self.preprocess_input is not None :
             processed = self.preprocess_input(self.target_img) 
        else :
             processed = np.array([self.target_img]) 
        self.predicted =  self.target_model.predict( processed )[0].argsort()[-5:][::-1] 
        self.width = processed.shape[2]
        self.height = processed.shape[1]      
        self.flag_toolbox = False
        self.base_vectors = None  
        self.init_point=None, 
        self.set_base_vectors(self.base_vectors) 
        self.setup_toolbox()
        self.set_init_point(self.init_point)  
        self.evol(self.num_gen, self.sigma, self.verbose)
        adv     = self.get_adv( self.hof[0] )
        adv_img = self.add_adv( adv )
        adv_img = np.clip(adv_img, 0, 255)
        return adv_img.astype(np.uint8)

    def generate_adv_image(self, target_img ):
        if target_img.ndim < 4 :
            return self.__generate_adv_image(target_img)
        else :
            result = []
            for i in range (len(target_img)):
                result.append(self.__generate_adv_image( target_img[i] ))
            return np.array(result) 
