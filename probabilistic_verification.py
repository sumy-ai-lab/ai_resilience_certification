import numpy as np 
import math

class SampleGenerator:
    def __init__(self, name: str):
        self.name = name

    def get_sample(self, N: int) -> np.ndarray :
        raise NotImplementedError("Should have implemented this")

def adaptive_interval_testing(theta: float, 
                              eta: float, 
                              delta: float, 
                              sample_source: SampleGenerator) -> str:
    theta1_left = 0
    theta2_left = 0
    theta1_right = 0
    theta2_right = 0
    # adjust the delta
    n = 3 + max(0, math.log2(theta / eta)) + max(0, math.log2((1- theta - eta) /eta))
    delta_min = delta / n
    print("n = ", n, "; delta_min = ", delta_min)
    while True:
      
        theta1_left, theta2_left = create_interval(theta, eta, theta1_left, theta2_left, True)
        if theta2_left - theta1_left > eta:
            tester_ans = tester(theta1_left, theta2_left, delta_min, sample_source)
            if tester_ans is 'Yes':
                return 'Yes'
        
        theta1_right, theta2_right = create_interval(theta, eta, theta1_right, theta2_right, False)
        if  theta2_right - theta1_right > eta:
            tester_ans = tester(theta1_right, theta2_right, delta_min, sample_source)
            if tester_ans is 'No':
                return 'No'

        if theta2_right - theta1_right <= eta and theta2_left - theta1_left <= eta:
            return tester(theta, theta + eta, delta_min, sample_source)

def create_interval(theta: float, 
                    eta: float, 
                    theta1: float, 
                    theta2: float, 
                    left: bool) -> (float, float):
    if theta == 0 and left:
        return theta, theta + eta
    if theta == 1:
        print('Always true xD!')
        exit(1)
    if theta1 == 0 and theta2 == 0 and left:
        return 0, theta
    if theta1 == 0 and theta2 == 0 and not left:
        return theta + eta, 1
    alpha = theta2 - theta1
    if left:
        return theta2 - max(eta, alpha / 2), theta2
    return theta1, theta1 + max(eta, alpha / 2)

def tester(theta1: float, 
           theta2: float, 
           delta: float, 
           sample_source: SampleGenerator) -> str:
    if theta2 > 1:
        theta2 = 1
    if theta1 < 0:
        theta1 = 0
    eta1 = eta2 = 0
    if theta1 > 0 and theta2 > 0 :
        eta1 = (theta2 - theta1)/(1+math.sqrt(2/3*theta2/theta1))
        eta2 = (theta2 - theta1)-eta1
    # compute number of samples
    N = 1/((theta2 - theta1)**2) * math.log(1/(delta))*(math.sqrt(3*theta1)+math.sqrt(2*(theta2)))**2
    N = math.ceil(N)
    print("N = ", N, "; theta1 = ", theta1, "; theta2 = ", theta2)
    s = sample_source.get_sample(N)
    mean = np.mean(s)
    if mean <= theta1+eta1:
        return 'Yes'
    if mean > theta2-eta2:
        return 'No'
    return 'None'